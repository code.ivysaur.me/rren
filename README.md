# rren

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

A command-line batch renaming client for Windows, using TR1 regexes.

Supports ECMA regexes, string find/replace, and propercasing using globbing file masks. Uses TR1 regexes (so it semi-predates C++11).

Original project page: https://code.google.com/p/rren/


## Download

- [⬇️ rren.exe](dist-archive/rren.exe) *(24.50 KiB)*
- [⬇️ rren.cpp](dist-archive/rren.cpp) *(5.72 KiB)*
